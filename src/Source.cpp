#include <iostream>
#include "../inc/Header.hpp"

namespace kk 
{
	void vvod(int n, int mas[N][N])
	{
		std::cout << std::endl;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				std::cin >> mas[i][j];
		std::cout << std::endl;
	}

	void vivod(int n, int mas[N][N])
	{
		std::cout << std::endl;
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
				std::cout << mas[i][j] << " ";
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}

	int max(int n, int mas[N][N])
	{
		int maximum = INT_MIN;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				if (mas[i][j] > maximum)
					maximum = mas[i][j];
		return maximum;
	}

	int min(int n, int mas[N][N])
	{
		int minimum = INT_MAX;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				if (mas[i][j] < minimum)
					minimum = mas[i][j];
		return minimum;
	}

	bool stroka(int i, int n, int mas[N][N])
	{
		int t, k;
		for (int j = 0; j < n; j++)
		{
			t = mas[i][j];
			while (t != 0)
			{
				k = t % 10;
				if (k == 8)
					return true;
				t /= 10;
			}
		}
	}

	void sort(int n, int mas[N][N])
	{
		for (int i = 0; i < n; i++)
			if (stroka(i, n, mas) == true)
			{
				int tmp;
				for (int j = 0; j < n - 1; j++)
					for (int t = j + 1; t < n; t++)
						if (mas[i][j] > mas[i][t])
						{
							tmp = mas[i][j];
							mas[i][j] = mas[i][t];
							mas[i][t] = tmp;
						}
			}
	}
}